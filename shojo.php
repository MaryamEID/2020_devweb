<?php
    session_start();
    include('connex.inc.php');
    include('liste_mangas.inc.php');

    /*l'utilisateur veut s'identifier*/
    if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
        identification();
    }
    /*l'administrateur ajoute un titre*/
    else{
        if($_SESSION['statut']==1){
            $erreur=ajouter_manga('shojo');
        }
    }
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Ma manga-tech</title>
    <link rel="stylesheet" href="acceuil.css">
  </head>

  <body class="body_2">


 <?php
    include("header.inc.php");
?>


 <div class="description">
   <div class="head">
       <h3 class="entete">Description</h3>
       </div>
   <p>Le shojo japonais est d&eacute;stin&eacute; &agrave; un public plutôt feminin. Les histoires sont souvent peupl&eacute;es de jeunes adolescentes et le th&egrave;me pr&eacute;pond&eacute;rant du genre est la romance.</p>


 </div>



 <div class="liste_livre">
    <div class="head">
      <h3 class="entete">Liste</h3>
    </div>

    <?php
        afficher_liste('shojo');
    echo "</div>";
    ?>
   </div>



  <?php
 	if(isset($_SESSION['pseudo']) && isset($_SESSION['statut']) && ($_SESSION['statut']==1)){
        if(isset($erreur)){
         afficher_formulaire_ajout($erreur);
        }
        else{
         afficher_formulaire_ajout(null);
        }
 }
?>


 </div>


 <footer class="footer3">
      <p>
      Contact<br> lina.belhadj@etu.univ-st-etienne.fr/maryam.eid@etu.univ-st-etienne.fr</p>
    </footer>




  </body>
</html>
