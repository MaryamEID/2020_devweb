<?php
    session_start();

    include 'connex.inc.php';
    if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
        identification();
    }
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Ma manga-tech</title>
    <link rel="stylesheet" href="acceuil.css">
    <script src="espace_perso.js"></script>
  </head>

  <body>



<?php
    include('header.inc.php');

    if(isset($_SESSION['pseudo']) && isset($_SESSION['statut'])){

     echo '
        <div class="head">
          <h3 class="entete">Ma Manga-Tech</h3>
        </div>';
        $pdo = connex('mangatech');

        echo "<div class=\"collection\" id=\"collection\" >";
        if(isset($_GET['titre'])){
            try{
            $titre=$_GET['titre'];
            $pdo= connex("mangatech");
            $sql=$pdo->prepare("SELECT * FROM manga WHERE titre=:titre");
            $sql->bindParam(":titre",$titre);
            $sql->execute();
            $resultat=$sql->fetch(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }


        echo "<div class=contenu id=\"contenu\" > <img src=\"".$resultat['couverture']."\" alt=\"couverture\" class=\"image_liste\" /><p class =\"description_resultats\">
        <strong>Titre:</strong> ".$resultat['titre']."<br><strong>Auteur: </strong>".$resultat['auteur']."<br><strong>Année de parution: </strong>".$resultat['annee_parution']."<br><strong>Genre: </strong>".$resultat['genre']."<br>".$resultat['description']."</p>
        <br><input type=\"submit\" onclick=\"window.location.href='supprimer.php?id_collection=".$resultat['ID']."'\" name=\"supprimer\" value=\"Supprimer de ma collection\" class=\"button1\"/></div>";
        echo "<script>afficher_titre();</script>";
    }
    echo "</div>";
        try{
        $pseudo=$_SESSION['pseudo'];
        $pdo= connex("mangatech");
        $sql=$pdo->prepare("SELECT * FROM manga,collection WHERE manga.ID=collection.ID_manga AND collection.pseudo=:pseudo");
        $sql->bindParam(":pseudo",$pseudo);
        $sql->execute();
    }
    catch(PDOException $e){
        echo $e->getMessage();
    }



        echo "<div class=\"liste_livre\">
        <div class=\"block_espace_perso\">";

        foreach($sql as $manga){
            echo "<div class=contenu> <a href=\"Espace_perso.php?titre=".$manga['titre']."\"><img src=\"".$manga['couverture']."\" alt=\"couverture\" class=\"image_liste\"  /></a> <p class=\"titre_manga\">".$manga['titre']."</p> ";
            echo "</div>";
        }


        echo '</div> ';
    echo "</div>";

    }
    else{
      echo '<div class="mauvais_acces">

     <p>Vous n\'avez pas accès à cette page.</p>

  </div>';
    }
?>




    <footer>
      <p>
      Contact<br> lina.belhadj@etu.univ-st-etienne.fr/maryam.eid@etu.univ-st-etienne.fr</p>
    </footer>
  </body>
</html>
