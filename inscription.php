<?php
    session_start();
    include 'connex.inc.php';
    if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
        /*l'utilisateur se connecte*/
            identification();
        }

        /*l'utilisateur s'inscrit*/
        if(isset($_POST['inscription']) && isset($_POST['pseudo']) && isset($_POST['mdp']) && isset($_POST['mail']) && isset($_POST['civilite'])
    && isset($_POST['jour']) && isset($_POST['mois'])&& isset($_POST['annee'])){
            $pdo= connex("mangatech");
            try{
                $pseudo=trim($_POST['pseudo']);
                $mdp=md5(trim($_POST['mdp']));
                $mail=trim($_POST['mail']);
                $civilite=$_POST['civilite'];
                $date_naissance=$_POST['annee']."-".$_POST['mois']."-".$_POST['jour'];
                $requete=$pdo->prepare("SELECT * FROM membres WHERE pseudo=:pseudo OR mail=:mail");
                $requete->bindParam(":pseudo",$pseudo);
                $requete->bindParam(":mail",$mail);
                $requete->execute();


                $n=$requete->fetchAll(PDO::FETCH_ASSOC);
                if(count($n)==0){
                    $sql = $pdo->prepare("INSERT INTO membres VALUES(:pseudo,:mdp,:mail, :date_naissance, :civilite, 0)");
                    $sql->bindParam(":pseudo",$pseudo);
                    $sql->bindParam(":mdp",$mdp);
                    $sql->bindParam(":civilite",$civilite);
                    $sql->bindParam(":mail",$mail);
                    $sql->bindParam(":date_naissance",$date_naissance);
                    $sql->execute();
                    $ok=1;

                }
                else{
                    echo "<script type=\"text/javascript\">window.alert(\"Cet utilisateur existe déjà.\");</script>";
                }
            }
            catch(PDOException $e){
                echo $e->getMessage();

            }
            if($ok==1){
            echo "<script type=\"text/javascript\">alert(\"Vous avez bien été inscrit. Vous pouvez à présent vous connecter.\");
            window.location.href='Acceuil.php';</script> ";
        }
        }

?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Ma manga-tech</title>
	<link rel="stylesheet" href="acceuil.css">
	<script src="inscription.js"></script>
  </head>

  <body class="body_2" onload="date_formulaire()">



<?php
    include('header.inc.php');


	if(isset($_SESSION['pseudo']) || isset($_SESSION['statut'])){
        header("Location:Acceuil.php");
	}

?>
		<div class="ligne">
		<div class="colonne">
		<form onsubmit="return verifier_donnees()" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"  >
			<fieldset>
			<legend><strong>Inscription</strong></legend>
			<label><input type="radio" id="mr" name="civilite" value="homme"/>Monsieur</label>
			<label><input type="radio" id="mme" name="civilite" value="femme"/>Madame</label>
		<br>
		Date de naissance <select id="jour" name="jour"></select> <select id="mois" name="mois"></select><select id="annee" name="annee"></select>

            <br>

			<label>Pseudo<input type="text" name="pseudo" <?php if(!isset($_POST["pseudo"])){
            echo "placeholder=\"Choisir un pseudo\"";
        }
            else{
                echo "value=\"".$_POST["pseudo"]."\"";
            }?>
            required="required"/></label><br>
			<label>Adresse mail<input type="email" name="mail"<?php if(isset($_POST["mail"])){
            echo "value=\"".$_POST["mail"]."\"";
        }?> required="required"/></label><br>
			<label>Mot de Passe<input type="password" name="mdp" required="required"/></label><br>

			<input type="submit" name="inscription" value="S'inscrire"class="button1"/>
			<input type="reset" value="Annuler"class="button1"/>
			</fieldset>
			</form>
		</div>

		<div class="colonne">
		<p><strong> Bienvenue sur Ma Manga-Tech!!!</strong> <br> Vous aussi vous êtes un fan de manga! Ce site est fait pour vous!<br>
			Ici vous pourrez:

		<ul>
		<li>Confectionner votre répertoire des mangas que vous avez lus.</li>
		<li>Ajout&eacute; les dans votre liste personnels</li>
		</ul>

		</p>
		</div>

		</div>




 <footer class="footer2">
      <p>
      Contact<br> lina.belhadj@etu.univ-st-etienne.fr/maryam.eid@etu.univ-st-etienne.fr</p>
    </footer>


  </body>
</html>
