function date_formulaire(){
    var i,j;
    for(i=0;i<31;i++){
        document.getElementById("jour").options[i]=new Option(i+1,i+1);
    }
    var mois = ["janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"];
    for (i = 0; i < mois.length; i++) {
        document.getElementById('mois').options[i] = new Option(mois[i],i+1);
    }
    i=0;
    for (j = 1950;j < 2007;j++) {
        document.getElementById('annee').options[i] = new Option(j,j);
        i++;
        }
}

function verifier_donnees(){

    var select=document.getElementById("jour");
     var jour_choix = select.selectedIndex;
     var jour = select.options[jour_choix].value;
     var select=document.getElementById("mois");
      var mois_choix = select.selectedIndex;
      var mois = select.options[mois_choix].text;
      var select=document.getElementById("annee");
       var annee_choix = select.selectedIndex;
       var annee = select.options[annee_choix].value;

     var tab_mois=["février","avril","juin","septembre","novembre"];
     if(tab_mois.includes(mois) && jour>30){
         alert("Veuillez entrer une date de naissance valide.");
         return false;
     }
     else if((( (!(annee % 4) && annee % 100) || (annee % 400) )) && (mois=tab_mois[1] && jour>28) ){
         alert("Veuillez entrer une date de naissance valide.");
         return false;
     }
}
