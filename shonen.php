<?php
    session_start();
    include('connex.inc.php');
    include('liste_mangas.inc.php');

    /*l'utilisateur veut s'identifier*/
    if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
        identification();
    }
    /*l'administrateur ajoute un titre*/
    else{
        if($_SESSION['statut']==1){
            ajouter_manga('shonen');
        }
    }
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Ma manga-tech</title>
    <link rel="stylesheet" href="acceuil.css">
  </head>

  <body class="body_2">

<?php
include("header.inc.php");
?>

 <div class="description">
   <div class="head">
       <h3 class="entete">Description</h3>
       </div>
   <p>Le shonen japonais est d&eacute;stin&eacute; &agrave; un public plutôt masculin.Le nom "shonen" d&eacute;signe justement la cat&eacute;gorie de jeune ado entre 8 et 18 ans. Le genre shonen regroupe les mangas les plus populaires tels que DragonBAll ou Hunter x Hunter.Les histoires metent en scenes des heros cherchant &agrave; se d&eacute;passer au cour d'une quête de nature souvent irrealisable (One piece 900 &eacute;pisodes n'ont pas suffit &agrave; trouver le trésors). Les valeurs mis en avant sont le gout de l'effort, l'amiti&eacute;, le d&eacute;passement de soit. </p>


 </div>



 <div class="liste_livre">
    <div class="head">
      <h3 class="entete">Liste</h3>
    </div>
        <?php
            afficher_liste('shonen');
        echo "</div>";
        ?>
   </div>


   <?php
   if(isset($_SESSION['pseudo']) && isset($_SESSION['statut']) && ($_SESSION['statut']==1)){
       if(isset($erreur)){
 		afficher_formulaire_ajout($erreur);
    }
    else{
        afficher_formulaire_ajout(null);
    }
    }
?>


 <footer class="footer3">
      <p>
      Contact<br> lina.belhadj@etu.univ-st-etienne.fr/maryam.eid@etu.univ-st-etienne.fr</p>
    </footer>




  </body>
</html>
