<?php
    session_start();
    include('connex.inc.php');
    include('liste_mangas.inc.php');

    /*l'utilisateur veut s'identifier*/
    if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
        identification();
    }
    /*l'administrateur ajoute un titre*/
    else{
        if($_SESSION['statut']==1){
            $erreur=ajouter_manga('seinen');
        }
    }
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Ma manga-tech</title>
    <link rel="stylesheet" href="acceuil.css">
  </head>

  <body class="body_2">


<?php
    include("header.inc.php");
?>


 <div class="description">
   <div class="head">
       <h3 class="entete">Description</h3>
       </div>
   <p>Le seinen manga est déstiné à un public de jeune adulte. Les scènes y sont plus violente et réaliste.</p>


 </div>



 <div class="liste_livre">
    <div class="head">
      <h3 class="entete">Liste</h3>
    </div>
    <?php
        afficher_liste('seinen');
    echo "</div>";
    ?>

   </div>



   <?php
 		if(isset($_SESSION['pseudo']) && isset($_SESSION['statut']) && ($_SESSION['statut']==1)){
            if(isset($erreur)){
             afficher_formulaire_ajout($erreur);
            }
            else{
             afficher_formulaire_ajout(null);
            }
     }
?>


 <footer class="footer3">
      <p>
      Contact<br> lina.belhadj@etu.univ-st-etienne.fr/maryam.eid@etu.univ-st-etienne.fr</p>
    </footer>




  </body>
</html>
