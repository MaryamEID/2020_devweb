<?php
function connex($base){
  /*include_once("param.inc.php");*/
  try {
      $pdo = new PDO('mysql:host=localhost;dbname='.$base, "root", "");
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }
  catch(PDOException $e) {
      echo 'Problème à la connexion';
      die();
  }

  return $pdo;
}

function identification(){
    if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
        if(isset($_POST['identification']) && isset($_POST['pseudo']) && isset($_POST['mdp'])){

            $pdo= connex("mangatech");

            try{

                $pseudo=trim($_POST['pseudo']);
                $mdp=md5(trim($_POST['mdp']));
                $sql = $pdo->prepare("SELECT * FROM membres WHERE pseudo=:pseudo AND mdp=:mdp");
                $sql->bindParam(":pseudo",$pseudo);
                $sql->bindParam(":mdp",$mdp);
                $sql->execute();
                $n=$sql->fetchAll(PDO::FETCH_ASSOC);

                if(count($n)==1){
                    $_SESSION['pseudo']=$pseudo;
                    $_SESSION['statut'] = intval($n[0]['statut']);
                    header('Location:Acceuil.php');
                }
                else{
                    echo "<script type=\"text/javascript\">window.alert(\"Pseudo ou mot de passe incorrect.\");</script>";
                }
            }
            catch(PDOException $e){
                echo $e->getMessage();

            }
        }
    }


}
?>
